module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  "pwa": {
    "manifestCrossorigin": "anonymous",
    "name": "Pérez Brown",
    "themeColor": "#021041",
    "background_color": "#ffffff",
  }
}