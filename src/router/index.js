import Vue from 'vue'
import VueRouter from 'vue-router'
import Lenguage from '../views/Lenguage.vue'
import MenuEs from '../views/navigation/MenuEs.vue'
import HomeEs from '../views/es/Home.vue'
import ServicesEs from '../views/es/Services.vue'
import UsEs from '../views/es/Us.vue'
import BlogEs from '../views/es/Blog.vue'
import BlogDetailEs from '../views/es/BlogDetail.vue'
import ContactEs from '../views/es/Contact.vue'
import LegalEs from '../views/es/Legal.vue'
import MenuEn from '../views/navigation/MenuEn.vue'
import HomeEn from '../views/en/Home.vue'
import ServicesEn from '../views/en/Services.vue'
import UsEn from '../views/en/Us.vue'
import BlogEn from '../views/en/Blog.vue'
import BlogDetailEn from '../views/en/BlogDetail.vue'
import ContactEn from '../views/en/Contact.vue'
import LegalEn from '../views/en/Legal.vue'
Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Lenguage',
    component: Lenguage
  },
  {
    path: '/es',
    component: MenuEs,
    children: [
      {
        path: '',
        component: HomeEs,
      },
      {
        path: 'services',
        name: 'Services',
        component: ServicesEs,
      },
      {
        path: 'us',
        name: 'us',
        component: UsEs,
      },
      {
        path: 'blog',
        name: 'blog',
        component: BlogEs,
      },
      {
        path: 'blog-articles/:name',
        name: 'blog-articles',
        component: BlogDetailEs,
      },
      {
        path: 'contact',
        name: 'contact',
        component: ContactEs,
      },
      {
        path: 'legal/:name',
        name: 'legal',
        component: LegalEs,
      }
    ]
  },
  {
    path: '/en',
    component: MenuEn,
    children: [
      {
        path: '',
        name: 'HomeEn',
        component: HomeEn,
      },
      {
        path: 'services',
        name: 'ServicesEn',
        component: ServicesEn,
      },
      {
        path: 'us',
        name: 'usEn',
        component: UsEn,
      },
      {
        path: 'blog',
        name: 'blogEn',
        component: BlogEn,
      },
      {
        path: 'blog-articles/:name',
        name: 'blog-articlesEn',
        component: BlogDetailEn,
      },
      {
        path: 'contact',
        name: 'contactEn',
        component: ContactEn,
      },
      {
        path: 'legal/:name',
        name: 'legalEn',
        component: LegalEn,
      }
    ]
  }
]

const router = new VueRouter({
  routes,
  mode: 'history',
  scrollBehavior () {
    return { x: 0, y: 0 };
  }
})

export default router
