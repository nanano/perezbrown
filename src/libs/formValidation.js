export const formValidation = {
    name: "formValidation",
    data() {
        return {
            formValid: true,
            formValidSubs: true,
            formError: {
                state: false,
                msg: '',
                timeout: 6000,
                color: 'error'
            },
            formRules: {
                required: [
                    v => !!v || 'El campo es requerido'
                ],
                text: [
                    v => !!v || 'El campo es requerido',
                    v => /^[.,\\A-Za-z0-9áéíóúÁÉÍÓÚÑñ ]{0,}$/.test(v) || 'El contenido no es válido, No se permiten símbolos especiales',
                    v => /\S/.test(v) || 'No se permiten solo espacios'
                ],
                email: [
                    v => !!v || 'El campo es requerido',
                    v => /.+@.+\..+/.test(v) || 'El correo no es válido',
                ],
                numbers: [
                    v => !!v || 'El campo es requerido',
                    v => /^[.0-9]{0,}$/.test(v) || 'Solo se permiten números',
                ],
                smallDecimal: [
                    v => !!v || 'El campo es requerido',
                    v => /^([0-9]{0,1})+([.]{1,1})+([0-9]{2,2})$/.test(v) || 'Debe ser decimal Ej. 1.74',
                    v => v.length == 4 || 'la longitud carácteres debe ser igual 4'
                ],
                int: [
                    v => !!v || 'El campo es requerido',
                    v => /^[0-9]{6,15}$/.test(v) || 'Solo se permiten números',
                ],
                phone: [
                    v => !!v || 'El campo es requerido',
                    v => /^[.0-9]{10,10}$/.test(v) || 'mínimo 10 numeros y solo se permiten números ',
                ],
                TwoPhones: [
                    v => !!v || 'El campo es requerido',
                    v => /^[.0-9]{7,10}$/.test(v) || 'mínimo 7 numeros y solo se permiten números ',
                ],
                hour: [
                    v => !!v || 'El campo es requerido',
                    v => /^ *(1[0-2]|[1-9]):[0-5][0-9] *(a|p|A|P)(m|M) *$/.test(v) || 'La hora no es válida',
                ],
                password: [
                    v => !!v || 'El campo es requerido',
                    v => (v && v.length >= 6) || 'La contraseña debe ser de 6 digitos o más'
                ],
                samePass: [
                    v => !!v || 'El campo es requerido',
                    v => this.password === v || 'La contraseña no es la misma'
                ]
            },
            formRulesEn: {
                required: [
                    v => !!v || 'The field is required'
                ],
                text: [
                    v => !!v || 'The field is required',
                    v => /^[.,\\A-Za-z0-9áéíóúÁÉÍÓÚÑñ ]{0,}$/.test(v) || 'The content is invalid',
                    v => /\S/.test(v) || 'The content is invalid'
                ],
                email: [
                    v => !!v || 'The field is required',
                    v => /.+@.+\..+/.test(v) || 'El email is invalid',
                ],
                numbers: [
                    v => !!v || 'The field is required',
                    v => /^[.0-9]{0,}$/.test(v) || 'Only numbers allowed',
                ],
                smallDecimal: [
                    v => !!v || 'El campo es requerido',
                    v => /^([0-9]{0,1})+([.]{1,1})+([0-9]{2,2})$/.test(v) || 'Debe ser decimal Ej. 1.74',
                    v => v.length == 4 || 'la longitud carácteres debe ser igual 4'
                ],
                int: [
                    v => !!v || 'El campo es requerido',
                    v => /^[0-9]{6,15}$/.test(v) || 'Solo se permiten números',
                ],
                phone: [
                    v => !!v || 'El campo es requerido',
                    v => /^[.0-9]{10,10}$/.test(v) || 'mínimo 10 numeros y solo se permiten números ',
                ],
                TwoPhones: [
                    v => !!v || 'El campo es requerido',
                    v => /^[.0-9]{7,10}$/.test(v) || 'mínimo 7 numeros y solo se permiten números ',
                ],
                hour: [
                    v => !!v || 'El campo es requerido',
                    v => /^ *(1[0-2]|[1-9]):[0-5][0-9] *(a|p|A|P)(m|M) *$/.test(v) || 'La hora no es válida',
                ],
                password: [
                    v => !!v || 'El campo es requerido',
                    v => (v && v.length >= 6) || 'La contraseña debe ser de 6 digitos o más'
                ],
                samePass: [
                    v => !!v || 'El campo es requerido',
                    v => this.password === v || 'La contraseña no es la misma'
                ]
            }
        }
    },
    methods: {
        alertError(color, msg) {
            this.formError.msg = msg;
            this.formError.state = true;
            this.formError.color = color;
        }
    }
}
