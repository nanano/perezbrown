import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import StoryblokVue from 'storyblok-vue';
import VueCarousel from 'vue-carousel';
import Page from '@/components/Page';
import VueMarkdown from 'vue-markdown';
import firebase from 'firebase';
//PAGES IMPORT
import lenguageComponent from '@/components/lenguage/Hero';
import HomeHeroImageComponent from '@/components/home/heroGallery/HeroGallery'
import HomeCasesComponent from '@/components/home/experience/Experience'
import HomeMembershipsComponent from '@/components/home/memberships/Memberships'
import LastBlogsComponent from '@/components/home/lastBlogs/LastBlogs'
import MainBannerComponent from '@/components/general/mainBanner/MainBanner';
import InfoTileComponent from '@/components/general/infoTile/InfoTile';
import ImageTextComponent from '@/components/general/imageText/ImageText';
import ImageBoxComponent from '@/components/general/imageBox/ImageBox';
import TextBoxComponent from '@/components/general/textBox/TextBox';
import ServicesComponent from '@/components/services/Services';
import InternationalComponent from '@/components/us/International';
import BlogFeaturedComponent from '@/components/blog/featured/Featured';
import BlogArticlesComponent from '@/components/blog/blogArticles/BlogArticles';
import BlogDetailComponent from '@/components/blog/blogDetail/BlogDetail';
import ContactFormComponent from '@/components/contact/ContactForm';

Vue.config.productionTip = false;

var firebaseConfig = {
  apiKey: "AIzaSyBN7S62e_PSjOYcvqTE46q_-rkx11qxz40",
  authDomain: "perezbrown-8f39b.firebaseapp.com",
  databaseURL: "https://perezbrown-8f39b.firebaseio.com",
  projectId: "perezbrown-8f39b",
  storageBucket: "perezbrown-8f39b.appspot.com",
  messagingSenderId: "138552218428",
  appId: "1:138552218428:web:01d20fa1f8807a9f5a8439"
};

firebase.initializeApp(firebaseConfig);

Vue.component('page', Page)
//PAGES
Vue.component('lenguageComponent', lenguageComponent);
Vue.component('HomeHeroImageComponent', HomeHeroImageComponent);
Vue.component('HomeCasesComponent', HomeCasesComponent);
Vue.component('HomeMembershipsComponent', HomeMembershipsComponent);
Vue.component('LastBlogsComponent', LastBlogsComponent);
Vue.component('MainBannerComponent', MainBannerComponent);
Vue.component('InfoTileComponent', InfoTileComponent);
Vue.component('ServicesComponent', ServicesComponent);
Vue.component('ImageTextComponent', ImageTextComponent);
Vue.component('ImageBoxComponent', ImageBoxComponent);
Vue.component('TextBoxComponent', TextBoxComponent);
Vue.component('InternationalComponent', InternationalComponent);
Vue.component('BlogFeaturedComponent', BlogFeaturedComponent);
Vue.component('BlogArticlesComponent', BlogArticlesComponent);
Vue.component('BlogDetailComponent', BlogDetailComponent);
Vue.component('ContactFormComponent', ContactFormComponent);

Vue.use(VueCarousel);
Vue.use(VueMarkdown);
Vue.use(StoryblokVue);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
